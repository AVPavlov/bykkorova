package bykKorova;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable{

    // свойства с аннотацией @FXML отражают элементы управления в окне
    @FXML
    private TableView myAttempts, userAttempts;
    @FXML
    private TextField userAttempt, bulls, cows;
    @FXML
    private Button userAttemptButton, userAnswerButton;
    @FXML
    private Label myTurnLabel, userTurnLabel, myAttempt, myAnswer;

    // списки попыток для человека и для программы
    private ObservableList<AttemptData> myAttemptData = FXCollections.observableArrayList(),
                                        userAttemptData = FXCollections.observableArrayList();

    // число ходов человека и программы
    private int userMoves = 0, myMoves = 0;

    // угадано ли
    private boolean userDone = false, computerDone = false;

    // загадатель
    private Riddler riddler;
    private Guesser guesser;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        // привязка полей свойств объекта AttemptData к столбцам формы
        final String[] names = {"no", "number", "result"};
        TableView[] tables = {myAttempts, userAttempts};
        for (TableView t : tables)
            for (int i = 0 ; i < 3; i++){
                ((TableColumn) t.getColumns().get(i))
                        .setCellValueFactory(new PropertyValueFactory<AttemptData, String>(names[i]));
            }

        // привязка источников данных к таблицам попыток
        myAttempts.setItems(myAttemptData);
        userAttempts.setItems(userAttemptData);

        // генерация числа
        riddler = new Riddler();

        // инициализация угадателя
        guesser = new Guesser();
    }

    /**
     * Человек отгадывает
     */
    @FXML
    public void onUserAttemptButton(ActionEvent e){
        String txt = userAttempt.getText();
        if (!isValidAttempt(txt)) {
            myAnswer.setText("4 цифры!");
            userAttempt.requestFocus();
        }
        else {
            userMoves++;
            myAnswer.setText("");
            userAttempt.clear();
            int intAttempt[] = new int[4];
            for (int i=0; i<4; i++) {
                intAttempt[i] = txt.charAt(i)-'0';
            }
            Result res = riddler.ask(intAttempt);
            userAttemptData.add(new AttemptData("" + userMoves, txt, res.byk, res.korova));
            userDone = (res.byk == 4);
            if (!userDone) {
                myAttempt.setText( guesser.getNextAttempt() );
                toggleMove();
            }
        }
    }

    /**
     * Человек отвечает на вопрос программы
     */
    @FXML
    public void onUserAnswerButton(ActionEvent e){
        String txt = bulls.getText();
        int b, k;
        if (!txt.matches("[0-4]")) {
            bulls.requestFocus();
            return;
        }
        b = Integer.parseInt(txt);
        txt = cows.getText();
        if (!txt.matches("[0-4]") || b + Integer.parseInt(txt) > 4) {
            cows.requestFocus();
            return;
        }
        k = Integer.parseInt(txt);
        myMoves++;

        Result r = new Result();
        r.byk = b;
        r.korova = k;
        guesser.processAttempt(myAttempt.getText(), r);
        myAttemptData.add(new AttemptData("" + myMoves, myAttempt.getText(), b, k));

        if (!computerDone) {
            toggleMove();
        }
    }


    @FXML
    public void onQuit(ActionEvent e){
        Platform.exit();
    }


    /**
     * Возвращает true, если строка содержит ровно 4 различные цифры
     * @param s строка
     * @return boolean
     */
    public boolean isValidAttempt(String s){
        if (!s.matches("\\d{4}")) return false;
        for (int i = 0; i < s.length(); i++)
            for (int j=i+1; j < s.length(); j++)
                if (s.charAt(i) == s.charAt(j)) return false;
        return true;
    }


    private void toggleMove(){
        toggleLabels();
        boolean user = userAttempt.isDisabled();

        cows.setDisable(user);
        bulls.setDisable(user);
        userAnswerButton.setDisable(user);

        userAttempt.setDisable(!user);
        userAttemptButton.setDisable(!user);

        if (user) {
            userAttempt.requestFocus();
        } else {
            bulls.requestFocus();
        }
    }


    private void toggleLabels(){
        Label[] labels = {userTurnLabel, myTurnLabel};
        for (Label l: labels)
            l.setOpacity((l.getOpacity()== 1) ? 0.2 : 1);
    }

}
