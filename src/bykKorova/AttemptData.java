package bykKorova;

import javafx.beans.property.SimpleStringProperty;

/**
 * Created by pav on 07.11.2016.
 */
public class AttemptData {
    private final SimpleStringProperty no, number, result;

    public AttemptData(String no, String number, String result) {
        this.no = new SimpleStringProperty(no);
        this.number = new SimpleStringProperty(number);
        this.result = new SimpleStringProperty(result);
    }

    public AttemptData(String no, String number, int bulls, int cows) {
        this.no = new SimpleStringProperty(no);
        this.number = new SimpleStringProperty(number);
        this.result = new SimpleStringProperty("" + bulls + "Б " + cows + "К");
    }

    public String getNo() {
        return no.get();
    }

    public void setNo(String _no) {
        no.set(_no);
    }

    public String getNumber() {
        return number.get();
    }

    public void setNumber(String _number) {
        number.set(_number);
    }

    public String getResult() {
        return result.get();
    }

    public void setResult(String _result) {
        result.set(_result);
    }
}