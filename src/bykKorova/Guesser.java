package bykKorova;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pav on 14.11.2016.
 */
public class Guesser {

    private List<String> possibleNumbers;
    //private Map<String, Boolean> possible;
    private List<String> pastAttempts;
    private List<Result> pastResults;


    public Guesser(){
        // составить список возможных чисел
        possibleNumbers = buildAllPossibleNumbers();

        // пометить все числа из списка как возможные
        //        for (String s : possibleNumbers){
        //            possible.put(s, true);
        //        }

        pastAttempts = new ArrayList<String>();
        pastResults= new ArrayList<Result>();
    }

    static List<String> buildAllPossibleNumbers(){
        List<String> result = new ArrayList<String>();

        for (int a = 0; a < 10; a++){
            for (int b = 0; b < 10; b++){
                if (b==a) continue;
                for (int c = 0; c < 10; c++){
                    if (c==a || c==b) continue;
                    for (int d = 0; d < 10; d++){
                        if (d==a||d==b||d==c) continue;
                        result.add(""+a+b+c+d);
                    }
                }
            }
        }
        return result;
    }

    public String getNextAttempt(){
        return possibleNumbers.get(0);
    }

    Boolean isPossible(String number) {
        for (int i=0; i < possibleNumbers.size(); ++i ){
            if (number==possibleNumbers.get(i)){
                return true;
            };
        }
        return false;
    }

    public Result resultIf(String correctNumber, String attempt){
        // Функция возвращает результат в быках и коровах,
        // на вопрос attempt, если загадано число correctNumber,
        Result r = new Result();

        return r;
    }

    public void processAttempt(String number, Result r){
        pastAttempts.add(number);
        pastResults.add(r);

        for (int i=0; i < possibleNumbers.size(); i++){
            String num = possibleNumbers.get(i);
            Result res = resultIf(num, number);
            if (res != r) {
                possibleNumbers.remove(i);
                i--;
            }
        }
    }
}
