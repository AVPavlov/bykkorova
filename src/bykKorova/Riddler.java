package bykKorova;

/**
 * Загадывает число и отвечает на вопросы
 */

class Riddler {
    private int[] myNumber;

    Riddler(){
        makeNumber();
    }

    void makeNumber(){
        myNumber = new int[4];
        myNumber[0] = 4;
        myNumber[1] = 3;
        myNumber[2] = 2;
        myNumber[3] = 1;
    }

    public String getMyNumber(){
        return "" + myNumber[0] + myNumber[1] + myNumber[2] + myNumber[3];
    }

    public Result ask(int[] number){
        Result r = new Result();
        for (int i = 0; i < 4; ++i){
            for (int j = 0; j < 4; ++j){
                if (number[i] == myNumber[j]) {
                    if (i!=j) {
                        r.korova++;
                    } else {
                        r.byk++;
                    }
                }
            }
        }
        return r;
    }

    public Result askString(String attempt) {
        Result r = new Result();

        for (int i = 0; i < 4; ++i){
            for (int j = 0; j < 4; ++j){
                if (attempt.charAt(i) == getMyNumber().charAt(j)) {
                    if (i != j) {
                        r.korova++;
                    } else {
                        r.byk++;
                    }
                }
            }
        }

        return r;
    }

}
