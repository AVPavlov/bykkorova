package bykKorova;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("app.fxml"));
        primaryStage.setTitle("Быки и коровы");
        primaryStage.getIcons().add(new Image("/bykKorova/gnu-logo.png"));
        primaryStage.setMinWidth(415);
        primaryStage.setMinHeight(220);
        Scene scene = new Scene(root, 400, 500);

        primaryStage.setScene(scene);

        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
