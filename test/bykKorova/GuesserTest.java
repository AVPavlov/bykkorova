package bykKorova;

import junit.framework.TestCase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by pav on 14.11.2016.
 */
public class GuesserTest extends TestCase {
    private Guesser guesser;

    @Test
    public void testNextAttemptDifferent() throws Exception {
        guesser = new Guesser();
        String att1 = guesser.getNextAttempt();
        String att2 = guesser.getNextAttempt();
        assertNotEquals("Guesser не должен спрашивать два одинаковых числа подряд",
                        att1, att2);
    }

    @Test
    public void testBuildAllPossibleNumbers() {
        guesser = new Guesser();
        List<String> l = guesser.buildAllPossibleNumbers();
        assertNotNull("buildAllPossibleNumbers не должен возвращать null!", l);
        assertEquals("buildAllPossibleNumbers должен возвращать список длины 5040", l.size(), 5040);
    }


}