package bykKorova;

import junit.framework.TestCase;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;

/**
 * Created by pav on 14.11.2016.
 */
public class ControllerTest extends TestCase {


    @Test
    public void testIsValidAttempt1111() throws Exception {
        Controller c = new Controller();
        assertFalse("1111 не является допустимой попыткой!", c.isValidAttempt("1111"));
    }

    @Test
    public void testIsValidAttempt7814() throws Exception {
        Controller c = new Controller();
        assertTrue("7814 является допустимой попыткой!", c.isValidAttempt("7841"));
    }

}