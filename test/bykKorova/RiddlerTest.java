package bykKorova;

import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by pav on 14.11.2016.
 */
public class RiddlerTest extends TestCase {

    private Riddler riddler;

    @Before
    public void setUp() throws Exception {
        riddler = new Riddler();
    }

    @After
    public void tearDown() throws Exception {
        riddler = null;
    }

    @Test
    public void testMakeNumber4Digits() throws Exception {
        riddler.makeNumber();
        String num = riddler.getMyNumber();
        assertTrue(num.matches("[0-9]{4}"));
    }

    @Test
    public void testMakeNumberDifferentDigits() throws Exception {
        riddler.makeNumber();
        String num = riddler.getMyNumber();

        boolean equals = false;
        for (int i =0; i < num.length(); i++)
            for (int j = 0; j < i; j++) {
                if (num.charAt(i) == num.charAt(j)) equals = true;
            }

        assertFalse(equals);
    }


    @Test
    public void testAskStringReverse() throws Exception {
        riddler.makeNumber();
        String num = riddler.getMyNumber();

        Result r =
                riddler.askString("" + num.charAt(3) + num.charAt(2) +
                                       num.charAt(1) + num.charAt(0));

        assertEquals(4, r.korova);
        assertEquals(0, r.byk);
    }

    @Test
    public void testAskStringCorrectAnswer() throws Exception {
        riddler.makeNumber();

        Result r = riddler.askString(riddler.getMyNumber());

        assertEquals(0, r.korova);
        assertEquals(4, r.byk);
    }
}